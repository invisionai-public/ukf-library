#pragma once

namespace UKF
{

/* Define the floating-point precision to use. */
using real_t = double;

}
